#!/bin/sh
#
# History:
#	2013/06/24 - [Tao Wu] Create file
#	2015/10/15 - [Tao Wu] Update file
#
# Copyright (c) 2015 Ambarella, Inc.
#
# This file and its contents ("Software") are protected by intellectual
# property rights including, without limitation, U.S. and/or foreign
# copyrights. This Software is also the confidential and proprietary
# information of Ambarella, Inc. and its licensors. You may not use, reproduce,
# disclose, distribute, modify, or otherwise prepare derivative works of this
# Software or any portion thereof except pursuant to a signed license agreement
# or nondisclosure agreement with Ambarella, Inc. or its authorized affiliates.
# In the absence of such an agreement, you agree to promptly notify and return
# this Software to Ambarella, Inc.
#
# THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
# INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF NON-INFRINGEMENT,
# MERCHANTABILITY, AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
# IN NO EVENT SHALL AMBARELLA, INC. OR ITS AFFILIATES BE LIABLE FOR ANY DIRECT,
# INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; COMPUTER FAILURE OR MALFUNCTION; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#

HOSTAP_VERSION="1.0"

encryption=$1
ssid=$2
passwd=$3
channel=$4

DEVICE=wlan0
DIR_CONFIG=/tmp/config

############# HOSTAP CONFIG ###############
HOSTAP_CTRL_INTERFACE=/var/run/hostapd
HOST_CONFIG=$DIR_CONFIG/hostapd.conf
DEFAULT_CHANNEL=1
HOST_MAX_STA=5
AP_PIN=12345670

#############  DHCP Server ###############
LOCAL_IP=192.168.42.1
LOCAL_NETMASK=255.255.255.0
DHCP_IP_START=192.168.42.2
DHCP_IP_END=192.168.42.20

############### Module ID ################
MODULE_ID=AR6003
if [ -e /sys/module/bcmdhd ]; then
	MODULE_ID=BCMDHD
elif [ -e /sys/module/8189es ]; then
	MODULE_ID=RTL8189ES
fi

#####################################
usage()
{
	echo "Version: ${HOSTAP_VERSION}"
	echo "This script used to Setup/Stop WiFi AP mode with hostapd"
	echo "usage:            $0 [open|wpa|wps] <SSID> <Password> <Channel>"
	echo ""
	echo "Example:"
	echo "Setup AP[Open]:   $0 open <SSID> 0 <Channel>"
	echo "Setup AP[WPA]:    $0 wpa  <SSID> <Password> <Channel>"
	echo "Setup AP[WPA+WPS]:$0 wps  <SSID> <Password> <Channel>"
	echo "     [Control AP] # hostapd_cli -i<interface> [wps_pbc | wps_pin any <PIN>] (PIN:${AP_PIN})"
	echo "Stop AP mode:     $0 stop"
	echo ""
	echo "NOTICE: Using interface AP[${DEVICE}] by default, change it if necessary."
}

check_param()
{
	if [ ${encryption} != "open" ] && [ ${encryption} != "wpa" ] && [ ${encryption} != "wps" ] && [ ${encryption} != "stop" ]; then
		echo "Please Select Encryption [open|wpa|wps] or stop"
		exit 1;
	fi

	if [ ${encryption} == "wpa" ] || [ ${encryption} == "wps" ]; then
		len=${#passwd}
		if [ $len -lt 8 ]; then
			echo "Password length at least 8"
			exit 1
		fi
	fi

	if [ ${#channel} -gt 0 ]; then
		if [ ${channel} -gt 196 ] || [ ${channel}  -lt 1 ]; then
			echo "Your Channel is wrong(1 ~ 196), using Channel ${DEFAULT_CHANNEL} by default."
			channel=$DEFAULT_CHANNEL
		fi
	else
		echo "Using Channel ${DEFAULT_CHANNEL} by default."
		channel=$DEFAULT_CHANNEL
	fi
}

# kill process
kill_apps()
{
	#the max times to kill app
	KILL_NUM_MAX=10

	for app in "$@"
	do
		kill_num=0
		echo "Kill ${app}"
		while [ "$(pgrep ${app})" != "" ]
		do
			if [ $kill_num -eq $KILL_NUM_MAX ]; then
				echo "Please try execute \"killall ${app}\" by yourself"
				exit 1
			else
				killall -9 ${app}
			fi
			kill_num=$((kill_num+1));
		done
	done
}

stop_wifi_app()
{
	kill_apps NetworkManager hostapd dnsmasq
}

stop_ap()
{
	stop_wifi_app
	echo "HostAP Stop Finished."
}

generate_hostapd_conf()
{
	if [ -f ${HOST_CONFIG} ]; then
		## Use the saved config, Do not need generate new config except failed to connect.
		return ;
	fi
	mkdir -p $DIR_CONFIG

	echo "interface=${DEVICE}" > ${HOST_CONFIG}
	echo "ctrl_interface=${HOSTAP_CTRL_INTERFACE}" >> ${HOST_CONFIG}
	echo "beacon_int=100" >> ${HOST_CONFIG}
#	echo "dtim_period=1" >> ${HOST_CONFIG}
	echo "preamble=0" >> ${HOST_CONFIG}
	#general
	echo "ssid=${ssid}" >> ${HOST_CONFIG}
	echo "max_num_sta=${HOST_MAX_STA}" >> ${HOST_CONFIG}
	#frequency=$(($channel*5 +2407))
	echo "channel=${channel}" >> ${HOST_CONFIG}

	if [ ${encryption} != "open" ]; then
		# wpa
		echo "wpa=2" >> ${HOST_CONFIG}
		echo "wpa_key_mgmt=WPA-PSK" >> ${HOST_CONFIG}
		echo "wpa_pairwise=CCMP" >> ${HOST_CONFIG}
		echo "wpa_passphrase=${passwd}" >> ${HOST_CONFIG}

		if [ ${encryption} == "wps" ]; then
			#Add wps support into wpa
			#echo "ignore_broadcast_ssid=0" >> ${HOST_CONFIG}
			#echo "ap_setup_locked=0" >> ${HOST_CONFIG}
			echo "wps_state=2" >> ${HOST_CONFIG}
			echo "eap_server=1" >> ${HOST_CONFIG}
			echo "ap_pin=${AP_PIN}" >> ${HOST_CONFIG}
			echo "config_methods=label display push_button keypad ethernet" >> ${HOST_CONFIG}
			echo "wps_pin_requests=/var/run/hostapd.pin-req" >> ${HOST_CONFIG}
		fi
	fi

	#Realtek rtl8189es
	if [ ${MODULE_ID} == "RTL8189ES" ]; then
		echo "driver=nl80211" >> ${HOST_CONFIG}
		echo "hw_mode=g" >> ${HOST_CONFIG}
		echo "ieee80211n=1" >> ${HOST_CONFIG}
		echo "ht_capab=[SHORT-GI-20][SHORT-GI-40][HT40]" >> ${HOST_CONFIG}
		#echo "wme_enabled=1" >> ${HOST_CONFIG}
		#echo "wpa_group_rekey=86400" >> ${HOST_CONFIG}
	fi
}

start_dhcp_server()
{
## Start DHCP Server ##
	mkdir -p /var/lib/misc
	mkdir -p /etc/dnsmasq.d
	dnsmasq -i${DEVICE} --no-daemon --no-resolv --no-poll --dhcp-range=${DHCP_IP_START},${DHCP_IP_END},1h &
}

hostapd_start_ap()
{
	stop_wifi_app

	## Setup interface and set IP,gateway##
	echo "Using Interface AP:[${DEVICE}]"
	ifconfig ${DEVICE} ${LOCAL_IP} up
	route add default netmask ${LOCAL_NETMASK} gw ${LOCAL_IP}
	## Start Hostapd ##
	CMD="hostapd ${HOST_CONFIG} -B"
	echo "CMD=${CMD}"
	$CMD

	start_dhcp_server
	echo "HostAP Setup Finished."
}

clear_config()
{
	rm -rf ${HOST_CONFIG}
}

################   Main  ###################

## Usage
if [ $# -lt 1 ]; then
	usage
	exit 1
fi

## Stop WiFi
if [ $# -eq 1 ] && [ ${encryption} == "stop" ]; then
	stop_wifi_app
	ifconfig ${DEVICE} down
	exit 0
fi

## Start WiFi
if [ $# -gt 2 ]; then
	check_param
	clear_config
fi

if [ ${encryption} == "open" ] || [ ${encryption} == "wpa" ] || [ ${encryption} == "wps" ]; then
	generate_hostapd_conf
	hostapd_start_ap
else
	usage
fi

########################################
