/*
 * Automatically generated C config: don't edit
 * Ambarella Build Configuration
 * Wed May 11 12:17:31 2016
 */

#ifndef AMBA_AUTO_CONFIG
#define AMBA_AUTO_CONFIG

#define CONFIG_AMBARELLA_LIBXML2_SUPPORT 1
#define BUILD_AMBARELLA_SECURITY_PACKAGE 1
#define CONFIG_LINUX_KERNEL_VERSION "3.10"
#define CONFIG_DIGITAL_AMBDBUS 1
#define CONFIG_AMBARELLA_BLUEZ_TOOLS_SUPPORT 1
#define DSP_IAVRSVD_SIZE 0x1000000
#define CONFIG_AMBARELLA_NTP_SUPPORT 1
#define CONFIG_AMBARELLA_LIBIDN_SUPPORT 1
#define AMBOOT_UART_115200 1
#define BUILD_AMBARELLA_ORYX_AUDIO_DEVICE_PULSE 1
#define BUILD_AMBARELLA_UNIT_TESTS_AMBADEBUG 1
#define CONFIG_AMBARELLA_FUSE_SUPPORT 1
#define CONFIG_AMBOOT_ENABLE_SD 1
#define CONFIG_AMBARELLA_NCURSES_SUPPORT 1
#define BUILD_AMBARELLA_FIRMWARE 1
#define BUILD_AMBARELLA_ORYX_BIN_DIR "/usr/bin"
#define CONFIG_AMBARELLA_AUTO_TELNETD 1
#define BUILD_AMBARELLA_ORYX_SERVICE_NETWORK 1
#define BUILD_AMBARELLA_ORYX_LED 1
#define CONFIG_DIGITAL_SIGNATURE_LIBRARY_NATIVE 1
#define KERNEL_VIF "$(AMB_TOPDIR)/amboot/vif/linux.info"
#define CONFIG_AMBARELLA_WPA_SUPPLICANT_SUPPORT 1
#define CONFIG_AMBARELLA_SOXR_SUPPORT 1
#define CONFIG_AMBARELLA_LIBELF_SUPPORT 1
#define IAV_MEM_QPM_SIZE 0x0018000
#define BUILD_AMBARELLA_DEBUG 1
#define ADD ""
#define BUILD_AMBARELLA_UNIT_TESTS_PI_CSS5 1
#define IAV_MEM_MV_SIZE 0x0000000
#define CONFIG_AMBARELLA_CA_CERTIFICATES_SUPPORT 1
#define CONFIG_UNIT_TEST_IMGPROC_PARAM_DIR "$(FAKEROOT_DIR)/etc/idsp"
#define AMBOOT_DEV_NORMAL_MODE 1
#define BUILD_AMBARELLA_ORYX_STREAM 1
#define CONFIG_AMBARELLA_LIBARCHIVE_SUPPORT 1
#define GPIO_BCM_WL_REG_ON 102
#define IDSP_RAM_START 0x07000000
#define CONFIG_AMBARELLA_OPENSSL_SUPPORT 1
#define CONFIG_AMBARELLA_PAM_SUPPORT 1
#define BUILD_AMBARELLA_ORYX_AUDIO_DEVICE 1
#define BUILD_BROADCOM_WIFI_SDIO_MODULE_AP6234 1
#define BUILD_AMBARELLA_UNIT_TESTS_PACKAGE 1
#define CONFIG_AMBARELLA_INIT_LINUXRC 1
#define BUILD_AMBARELLA_ORYX_OSAL 1
#define BUILD_AMBARELLA_MIDDLEWARE 1
#define CONFIG_AMBOOT_ENABLE_NAND 1
#define CONFIG_NAND_1DEVICE 1
#define CONFIG_IMGPROC_MEM_SMALL 1
#define CONFIG_AMBARELLA_EXPAT_SUPPORT 1
#define CONFIG_BROADCOM_TOOL_SUPPORT 1
#define CONFIG_AMBARELLA_DNSMASQ_SUPPORT 1
#define CONFIG_AMBARELLA_ETH0 1
#define CONFIG_AMBARELLA_PREBUILD 1
#define BUILD_AMBARELLA_ORYX_SERVICE_FRAME 1
#define BUILD_AMBARELLA_UNIT_TESTS_BENCHMARK 1
#define BUILD_AMBARELLA_ORYX_UTILITY 1
#define BUILD_AMBARELLA_IAV 1
#define BUILD_AMBARELLA_ORYX_NETWORK 1
#define CONFIG_NAND_ERASE_UNUSED_BLOCK 1
#define CONFIG_IMAGE_DEVICE_INSTALL_DIR "$(AMB_TOPDIR)/prebuild/img_dev"
#define BUILD_AMBARELLA_ORYX_PLAYBACK_NEW_VIDEO_DECODER_AMBA 1
#define BUILD_AMBARELLA_ORYX_PLAYBACK_NEW_FFMPEG_LIBRARY 1
#define CONFIG_AMBARELLA_LIBTASN1_SUPPORT 1
#define CONFIG_AMBARELLA_ETH0_IP_MASK "255.255.255.0"
#define CONFIG_NAND_USE_FLASH_BBT 1
#define CONFIG_AMBARELLA_MOUNT_MNT ""
#define CONFIG_AMBARELLA_READLINE_SUPPORT 1
#define CONFIG_AMBARELLA_ASKFIRST_CONSOLE_NA 1
#define CONFIG_AMBARELLA_FREETYPE_SUPPORT 1
#define BUILD_AMBARELLA_VIDEO_PLUGIN_DPTZ 1
#define BUILD_AMBARELLA_MIDDLEWARE_PROC 1
#define BUILD_AMBARELLA_EXTERNAL_MODULES 1
#define BUILD_AMBARELLA_ORYX_PLAYBACK_NEW_DEMUXER_RTSP 1
#define CONFIG_DSP_FIRMWARE_GERNEIC 1
#define GPIO_BCM_WL_HOST_WAKE 4
#define SECONDARY ""
#define BUILD_AMBAREALLA_APP_UTILITY_UPGRADE 1
#define BUILD_AMBARELLA_ORYX_DEMUXER_DIR "/usr/lib/oryx/demuxer"
#define BUILD_AMBARELLA_APP_UTILITY_LOAD_UCODE 1
#define CONFIG_AMBARELLA_BUSYBOX_PREBUILD 1
#define BUILD_AMBARELLA_ORYX_PROTOCOL 1
#define CONFIG_AMBARELLA_HOSTAPD_LATEST_SUPPORT 1
#define CONFIG_AMBARELLA_ETH0_GW_ADDRESS "10.0.0.1"
#define BUILD_AMBARELLA_ORYX_PLAYBACK_NEW_VIDEO_RENDERER_AMBA 1
#define CONFIG_AMBARELLA_LIBOGG_SUPPORT 1
#define BUILD_AMBARELLA_UNIT_TESTS_BANDWIDTH 1
#define FRAMEBUFFER_SIZE 0x0000000
#define BUILD_AMBARELLA_ORYX_SERVICE_AUDIO 1
#define CONFIG_AMBARELLA_IPERF_SUPPORT 1
#define CONFIG_PRINT_DSP_CMD 1
#define CONFIG_UNIT_TEST_INSTALL_DIR "$(FAKEROOT_DIR)/usr/local/bin"
#define CONFIG_AMBARELLA_LIBEXOSIP2_SUPPORT 1
#define CONFIG_AMBARELLA_ETHTOOL_SUPPORT 1
#define BUILD_AMBARELLA_ORYX_MW 1
#define BUILD_AMBARELLA_ORYX_AUDIO_CAPTURE 1
#define RAMDISK_VIF ""
#define LNX "$(AMB_BOARD_OUT)/rootfs/ubifs"
#define CONFIG_CPU_CORTEXA9_HF 1
#define KERNEL_RAM_START 0x00208000
#define CONFIG_AMBARELLA_LIBPCRE_SUPPORT 1
#define BUILD_AMBARELLA_TEXTINSERT_PACKAGE 1
#define BUILD_AMBARELLA_ORYX_IMAGE_QUALITY_UNIT_TEST 1
#define CONFIG_AMBOOT_ENABLE_ETH 1
#define BUILD_AMBARELLA_ORYX_EVENT_PLUGIN_MOTION_DETECT 1
#define ROMFS_VIF ""
#define CONFIG_AMBARELLA_ZLIB_SUPPORT 1
#define BUILD_AMBARELLA_ORYX_SERVICE_VIDEO 1
#define CONFIG_AMBOOT_BD_CMDLINE ""
#define AMBOOT_BLD_RAM_START 0x00000000
#define CONFIG_SENSOR_OV4689_MIPI 1
#define CONFIG_AMBOOT_COMMAND_SUPPORT 1
#define CONFIG_AMBOOT_BD_FDT_SUPPORT 1
#define BUILD_AMBARELLA_PRIVATE_LINUX_UNIT_TESTS 1
#define CONFIG_AMBARELLA_PULSEAUDIO_SUPPORT 1
#define BUILD_AMBARELLA_APP_IPCAM_RTSP 1
#define CONFIG_AMBARELLA_LIBOPUS_SUPPORT 1
#define BUILD_AMBARELLA_ORYX_IPC_UNIT_TEST 1
#define BUILD_OPENSSL_WRAPPER_LIBRARY 1
#define ADD_VIF ""
#define CONFIG_AMBARELLA_LIBJPEG_TURBO_SUPPORT 1
#define AMBOOT_BLD_HEAP_SIZE 0x00008000
#define BUILD_AMBARELLA_APPLICATION 1
#define CONFIG_AMBARELLA_EXT_CMD_FILE ""
#define CONFIG_AMBARELLA_ALSA_UTILS_SUPPORT 1
#define BUILD_DEVFW 1
#define BUILD_AMBARELLA_ORYX_PLAYBACK_NEW 1
#define BUILD_AMBARELLA_ORYX_PLAYBACK_NEW_AUDIO_AMBA_AAC_LIBRARY 1
#define CONFIG_AMBARELLA_LIBSNDFILE_SUPPORT 1
#define BUILD_AMBARELLA_UNIT_TESTS_VIN 1
#define BUILD_AMBARELLA_BSREADER_PACKAGE 1
#define CONFIG_APP_IPCAM_CONFIG_DIR "$(FAKEROOT_DIR)/etc/ambaipcam"
#define CONFIG_AMBARELLA_ORYX_VIDEO_IAV2 1
#define CONFIG_AMBOOT_ENABLE_USB 1
#define CONFIG_AMBARELLA_LIBSEPOL_SUPPORT 1
#define CONFIG_AMBARELLA_LIBTOOL_SUPPORT 1
#define CONFIG_AMBARELLA_ALSA_PLUGINS_SUPPORT 1
#define CONFIG_AMBARELLA_NETTLE_SUPPORT 1
#define DSP_FASTAUDIO_SIZE 0x0000000
#define ARCH_SUPPORT_BCH_ID5 1
#define DSP_FASTDATA_SIZE 0x0000000
#define BUILD_AMBAREALLA_CLOUD_LIB 1
#define CONFIG_AMBARELLA_LIBUSB_COMPAT_SUPPORT 1
#define AMBARELLA_INITRD_PHYS 0x00800000
#define BUILD_AMBARELLA_ORYX_CONFIG_UNIT_TEST 1
#define BUILD_AMBARELLA_ORYX_SERVICE_PLAYBACK 1
#define CONFIG_AMBARELLA_DBUS_SUPPORT 1
#define CONFIG_IMGPROC_INSTALL_DIR "$(AMB_TOPDIR)/prebuild/imgproc"
#define CONFIG_FLASH_ERASE_128k 1
#define CONFIG_KERNEL_MODULES_INSTALL_DIR "$(FAKEROOT_DIR)"
#define CONFIG_NAND_P2K_E128K 1
#define BUILD_AMBARELLA_PUBLIC_LINUX_UNIT_TESTS 1
#define BUILD_AMBARELLA_PACKAGE 1
#define BUILD_AMBARELLA_UNIT_TESTS_IDSP 1
#define CONFIG_AMBARELLA_DBUS_GLIB_SUPPORT 1
#define CONFIG_AMBARELLA_WIRELESS_TOOL_SUPPORT 1
#define CONFIG_AMBARELLA_IMAGE_DATA 1
#define BUILD_AMBAREALLA_UTILITY_SCRIPTS_ALL 1
#define BUILD_AMBARELLA_ORYX_AUDIO_CAPTURE_PULSE 1
#define BUILD_AMBARELLA_DATATX_PACKAGE 1
#define BUILD_AMBARELLA_IMGPROC_PACKAGE 1
#define BUILD_AMBARELLA_ORYX_PROTOCOL_RTSP 1
#define CONFIG_BOOT_MEDIA_NAND 1
#define BUILD_AMBARELLA_ORYX_LIB_DIR "/usr/lib"
#define BUILD_AMBARELLA_ORYX_VIDEO 1
#define BUILD_AMBARELLA_UNIT_TESTS 1
#define CONFIG_AMBARELLA_PARTED_SUPPORT 1
#define BUILD_AMBARELLA_ORYX_SERVICE_MEDIA 1
#define BUILD_AMBARELLA_ORYX_EVENT_PLUGIN_DIR "/usr/lib/oryx/event"
#define DSP_BSB_SIZE 0x600000
#define CONFIG_AMBARELLA_EXFAT_UTILS_SUPPORT 1
#define BUILD_AMBARELLA_VIN 1
#define BUILD_AMBARELLA_ORYX_VIDEO_PLUGIN_DIR "/usr/lib/oryx/video"
#define CONFIG_AMBARELLA_BROADCOM_WIFI_SUPPORT 1
#define CONFIG_AMBARELLA_LIBSELINUX_SUPPORT 1
#define CONFIG_BSP_BOARD_S2LM_IRONMAN 1
#define CONFIG_AMBARELLA_MKFS 1
#define RAMDISK ""
#define CONFIG_AMBARELLA_LIBBZIP2_SUPPORT 1
#define KERNEL "$(AMB_BOARD_OUT)/kernel/Image"
#define CONFIG_AMBARELLA_ROOT_PASSWORD ""
#define CONFIG_AMBARELLA_LIBNDP_SUPPORT 1
#define CONFIG_MIDDLEWARE_INSTALL_DIR "$(AMBABUILD_TOPDIR)/prebuild/img_mw"
#define BUILD_AMBARELLA_DSP 1
#define BUILD_AMBARELLA_ORYX_SERVICE_IMAGE 1
#define WIFI_CONN_SD_SLOT_NUM 0
#define BUILD_AMBARELLA_ORYX_AUDIO 1
#define BUILD_AMBARELLA_LBR_PACKAGE 1
#define BUILD_AMBARELLA_ORYX_PLAYBACK_NEW_LINUXFB 1
#define CONFIG_AMBARELLA_LIBGCRYPT_SUPPORT 1
#define BUILD_BROADCOM_WIFI_SDIO 1
#define CONFIG_UTIL_LINUX_SUPPORT 1
#define BUILD_AMBARELLA_LINUX_KERNEL 1
#define BUILD_AMBARELLA_UNIT_TESTS_VSYNC 1
#define BUILD_AMBARELLA_ORYX_DEC7Z 1
#define CONFIG_AMBARELLA_JSONC_SUPPORT 1
#define CONFIG_DSP_LOG_START_0X80000 1
#define BUILD_AMBARELLA_VIDEO_PLUGIN_OVERLAY 1
#define CONFIG_AMBARELLA_FFMPEG_SUPPORT 1
#define CONFIG_AMBARELLA_UBI_TOOLS_SUPPORT 1
#define CONFIG_HDMI_NULL 1
#define CONFIG_PRINT_DSP_CMD_MORE 1
#define CONFIG_DVE_AMBTVE 1
#define SWP ""
#define CONFIG_AMBARELLA_GNUTLS_SUPPORT 1
#define BUILD_AMBARELLA_UNIT_TESTS_NBENCH 1
#define CONFIG_AMBARELLA_LIBG7XX_SUPPORT 1
#define BUILD_UPGRADE_FOR_3_10 1
#define BUILD_AMBARELLA_DEWARP_DYNAMIC 1
#define ROMFS ""
#define BUILD_AMBARELLA_LW_CRYPTOGRAPHY 1
#define BUILD_AMBARELLA_HW_TIMER 1
#define CONFIG_AMBARELLA_FREE_FONTS_SUPPORT 1
#define CONFIG_AMBARELLA_DHCPCD_SUPPORT 1
#define CONFIG_AMBARELLA_IW_SUPPORT 1
#define BUILD_AMBARELLA_APP_UTILITY_DSPLOG 1
#define CONFIG_AMBARELLA_DHCP_TOOL_DHCLIENT 1
#define CONFIG_AMBARELLA_ALSA_PLUGINS_PULSEAUDIO_SUPPORT 1
#define CONFIG_AMBARELLA_SPEEX_DSP_SUPPORT 1
#define CONFIG_AMBARELLA_LIBNL_SUPPORT 1
#define BUILD_AMBARELLA_ORYX_RTSP_SERVICE 1
#define CONFIG_AMBARELLA_LIBGPG_ERROR_SUPPORT 1
#define CONFIG_AMBARELLA_HOSTAPD_SUPPORT 1
#define BUILD_AMBARELLA_ORYX_PLAYBACK_NEW_DEMUXER_MP4 1
#define ARCH_SUPPORT_SPINOR 1
#define BUILD_AMBARELLA_DIGITAL_SIGNATURE 1
#define CONFIG_AMBARELLA_LIBUSBX_SUPPORT 1
#define BUILD_AMBARELLA_PRIVATE_MODULES 1
#define BUILD_AMBARELLA_ORYX_EVENT_PLUGIN_KEY_INPUT 1
#define AMBOOT_DEFAULT_SN ""
#define CONFIG_AMBARELLA_IPTABLES_3_8_SUPPORT 1
#define CONFIG_AMBARELLA_GMP_SUPPORT 1
#define PBA "$(AMB_BOARD_OUT)/kernel/pba_zImage"
#define BUILD_AMBARELLA_ORYX_JPEG_ENCODER 1
#define BUILD_AMBARELLA_ORYX_SIP_SERVICE 1
#define IAV_MEM_USR_SIZE 0x0000000
#define CONFIG_AMBARELLA_LIBOSIP2_SUPPORT 1
#define BUILD_AMBARELLA_ORYX_STREAM_RECORD 1
#define MINIMAL_FIRMWARE_COMBO 1
#define CONFIG_AMBARELLA_BLUETOOTH4_TOOL_SUPPORT 1
#define CONFIG_NAND_S34ML02G1 1
#define CONFIG_ARCH_S2L 1
#define CONFIG_AMBARELLA_START_WATCHDOG 1
#define IAV_MEM_VCA_SIZE 0x0000000
#define BUILD_AMBARELLA_ORYX_CODEC_DIR "/usr/lib/oryx/codec"
#define CONFIG_AMBARELLA_ELFUTILS_SUPPORT 1
#define CONFIG_DEWARP_INSTALL_DIR "$(AMBABUILD_TOPDIR)/prebuild/dewarp"
#define CONFIG_AMBOOT_ENABLE_UART0 1
#define BUILD_AMBARELLA_ORYX_WATCHDOG 1
#define BACKUP_VIF ""
#define BUILD_BROADCOM_WIFI_SDIO_V3_0 1
#define ADC ""
#define BUILD_AMBARELLA_ORYX_EVENT_PLUGIN_AUDIO_ALERT 1
#define CONFIG_AMBARELLA_ROOTFS_UBIFS 1
#define BUILD_AMBARELLA_ORYX_IPC 1
#define CONFIG_APP_INSTALL_DIR "$(FAKEROOT_DIR)/usr/local/bin"
#define BUILD_AMBARELLA_ORYX_SERVICE_EVENT 1
#define BUILD_AMBARELLA_SMARTCAM_PACKAGE 1
#define BUILD_AMBARELLA_APP_UTILITY_DSP 1
#define BACKUP ""
#define BUILD_AMBARELLA_ORYX_LIBARCHIVE 1
#define BUILD_AMBARELLA_ORYX_STREAM_PLAYBACK 1
#define BUILD_AMBARELLA_VIDEO_PLUGIN_LBR 1
#define BUILD_AMBARELLA_ORYX_UPGRADE 1
#define IAV_MEM_PM_SIZE_S2L 0x03C0000
#define CONFIG_AMBARELLA_SERIAL_PORT0_GETTY 1
#define BUILD_AMBARELLA_ORYX_CONF_DIR "/etc/oryx"
#define BUILD_AMBARELLA_APP_IPCAM 1
#define BUILD_AMBARELLA_ORYX_IMAGE_QUALITY 1
#define CONFIG_AMBARELLA_LIBSSH2_SUPPORT 1
#define BUILD_AMBARELLA_ORYX_SERVICES 1
#define BUILD_AMBARELLA_ORYX_PLAYBACK_NEW_AMBA_DSP 1
#define CONFIG_AMBARELLA_LIBCAP_SUPPORT 1
#define CONFIG_AMBARELLA_IAV_DRAM_WARP_MEM 1
#define BUILD_AMBARELLA_ORYX_MUXER_DIR "/usr/lib/oryx/muxer"
#define AMBOOT_BLD_STACK_SIZE 0x00008000
#define BUILD_AMBARELLA_VOUT 1
#define CONFIG_AMBARELLA_LIBCURL_SUPPORT 1
#define LNX_VIF "$(AMB_TOPDIR)/amboot/vif/ubifs.info"
#define PBA_VIF "$(AMB_TOPDIR)/amboot/vif/pba.info"
#define BUILD_AMBARELLA_ORYX_SERVICE_APPS_LAUNCHER 1
#define BUILD_AMBARELLA_ORYX_SERVICE_API_PROXY 1
#define BUILD_AMBAREALLA_APP_UTILITY 1
#define BUILD_AMBARELLA_ORYX_SERVICE_MANAGER 1
#define CONFIG_AMBARELLA_LIBFFI_SUPPORT 1
#define BUILD_AMBARELLA_DSP_LOG 1
#define SWP_VIF ""
#define BUILD_AMBARELLA_ORYX_SERVICE_SYSTEM 1
#define BUILD_AMBARELLA_ORYX_VIDEO_PLUGIN 1
#define CONFIG_KERNEL_DEFCONFIG_STRING "s2lmironman_kernel_config"
#define CONFIG_AMBARELLA_WEBRTC_AUDIO_PROCESSING_SUPPORT 1
#define CONFIG_FLASH_PAGE_2K 1
#define BUILD_AMBARELLA_IMAGE_DEVICE_PROC 1
#define CONFIG_AMBARELLA_RUN_DEMO_NONE 1
#define BUILD_AMBARELLA_ORYX_PLAYBACK_NEW_ALSA_LIBRARY 1
#define CONFIG_AMBARELLA_FUSE_EXFAT_SUPPORT 1
#define BUILD_AMBARELLA_ORYX_EVENTMONITOR 1
#define CONFIG_AMBOOT_ENABLE_GPIO 1
#define CONFIG_AMBARELLA_PROCPS_SUPPORT 1
#define CONFIG_AMBARELLA_ETH0_ADDRESS "10.0.0.2"
#define ADC_VIF ""
#define CONFIG_AMBARELLA_SPEEX_SUPPORT 1
#define CONFIG_AMBARELLA_NETWORKMANAGER_SUPPORT 1
#define CONFIG_AMBARELLA_LIBATTR_SUPPORT 1
#define CONFIG_AMBARELLA_BLUETOOTH_USB_SUPPORT 1
#define BUILD_AMBARELLA_ORYX_CFG_SET 1
#define BUILD_AMBARELLA_DEWARP_PACKAGE 1
#define BUILD_AMBAREALLA_UTILITY_SCRIPTS 1
#define BUILD_AMBARELLA_UTILITIES_PACKAGE 1
#define CONFIG_AMBARELLA_PULSEAUDIO_UTILS_SUPPORT 1
#define BUILD_AMBARELLA_ORYX_PROTOCOL_SIP 1
#define BUILD_AMBARELLA_SMARTCAM_PACKAGE_MDET 1
#define CONFIG_AMBARELLA_LUA_SUPPORT 1
#define AMBOOT_DEFAULT_LINUX_MACHINE_ID 4603
#define BUILD_AMBARELLA_ORYX_PLAYBACK_NEW_UNIT_TEST 1
#define BUILD_AMBARELLA_ORYX_FILTER_DIR "/usr/lib/oryx/filter"
#define CONFIG_AMBARELLA_GLIB_SUPPORT 1
#define BUILD_WIFI_BLUETOOTH_MODULES 1
#define BUILD_AMBARELLA_ORYX_CONFIG 1
#define CONFIG_AMBARELLA_SERIAL_PORT1_NA 1
#define IAV_MEM_OVERLAY_SIZE 0x0200000
#define BUILD_AMBARELLA_IMGPROC_DRV 1
#define CONFIG_BOARD_VERSION_S2LMIRONMAN_S2L55M 1
#define SECONDARY_VIF ""
#define BUILD_AMBARELLA_ORYX_UTIL 1
#define CONFIG_BACKUP_CPIO 1
#define CONFIG_AMBARELLA_ALSA_SUPPORT 1
#define BUILD_AMBARELLA_UNIT_TESTS_IAV 1
#define AMBOOT_DEV_AUTO_BOOT 1
#define CONFIG_AMBARELLA_DHCP_SUPPORT 1
#define CONFIG_AMBARELLA_WPA_SUPPLICANT_LATEST_SUPPORT 1

#endif  /* AMBA_AUTO_CONFIG */
