/*******************************************************************************
 * am_motion_detect_config.h
 *
 * History:
 *   Jan 22, 2015 - [binwang] created file
 *
 * Copyright (c) 2016 Ambarella, Inc.
 *
 * This file and its contents ("Software") are protected by intellectual
 * property rights including, without limitation, U.S. and/or foreign
 * copyrights. This Software is also the confidential and proprietary
 * information of Ambarella, Inc. and its licensors. You may not use, reproduce,
 * disclose, distribute, modify, or otherwise prepare derivative works of this
 * Software or any portion thereof except pursuant to a signed license agreement
 * or nondisclosure agreement with Ambarella, Inc. or its authorized affiliates.
 * In the absence of such an agreement, you agree to promptly notify and return
 * this Software to Ambarella, Inc.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF NON-INFRINGEMENT,
 * MERCHANTABILITY, AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL AMBARELLA, INC. OR ITS AFFILIATES BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; COMPUTER FAILURE OR MALFUNCTION; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/
#ifndef AM_MOTION_DETECT_CONFIG_H_
#define AM_MOTION_DETECT_CONFIG_H_

#include "am_log.h"
#include "am_base_event_plugin.h"
#include "am_event_types.h"
#include "am_configure.h"

#define MAX_ROI_NUM 4

struct MotionDetectParam
{
    bool enable;
    uint32_t buf_id;
    AM_EVENT_MD_ROI roi_info[MAX_ROI_NUM];
    AM_EVENT_MD_THRESHOLD th[MAX_ROI_NUM];
    AM_EVENT_MD_LEVEL_CHANGE_DELAY lc_delay[MAX_ROI_NUM];
    /*MotionDetectParam():
    enable(false),
    buf_id(0),
    roi_info[MAX_ROI_NUM]({{0,0,0,0,0,false},{1,0,0,0,0,false},{2,0,0,0,0,false},{3,0,0,0,0,false}}),
    th[MAX_ROI_NUM]({{0,0,0},{1,0,0},{2,0,0},{3,0,0}}),
    lc_delay[MAX_ROI_NUM]({{0,0,0},{1,0,0},{2,0,0},{3,0,0}})
    {
    }*/
};

class AMMotionDetectConfig
{
  public:
    AMMotionDetectConfig();
    virtual ~AMMotionDetectConfig();
    MotionDetectParam* get_config(const std::string& cfg_file_path);
    bool set_config(MotionDetectParam *md_config,
                    const std::string& cfg_file_path);
  private:
    AMConfig *m_config;
    MotionDetectParam *m_md_config;
};

#endif /* AM_MOTION_DETECT_CONFIG_H_ */
