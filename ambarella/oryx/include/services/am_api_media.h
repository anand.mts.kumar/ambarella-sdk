/*******************************************************************************
 * am_api_media.h
 *
 * History:
 *   2015-2-25 - [ccjing] created file
 *
 * Copyright (c) 2016 Ambarella, Inc.
 *
 * This file and its contents ("Software") are protected by intellectual
 * property rights including, without limitation, U.S. and/or foreign
 * copyrights. This Software is also the confidential and proprietary
 * information of Ambarella, Inc. and its licensors. You may not use, reproduce,
 * disclose, distribute, modify, or otherwise prepare derivative works of this
 * Software or any portion thereof except pursuant to a signed license agreement
 * or nondisclosure agreement with Ambarella, Inc. or its authorized affiliates.
 * In the absence of such an agreement, you agree to promptly notify and return
 * this Software to Ambarella, Inc.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF NON-INFRINGEMENT,
 * MERCHANTABILITY, AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL AMBARELLA, INC. OR ITS AFFILIATES BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; COMPUTER FAILURE OR MALFUNCTION; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/
/*! @file am_api_media.h
 *  @brief This header file contains a class used to add file to
 *         playback in the media service.
 */
#ifndef AM_API_MEDIA_H_
#define AM_API_MEDIA_H_

#include <string>
#include "commands/am_api_cmd_media.h"

/*! @defgroup airapi-datastructure-media Data Structure of Media Service
 *  @ingroup airapi-datastructure
 *  @brief All Oryx Media Service related method call data structures
 *  @{
 */

/*! @class am_api_playback_audio_file_list_t
 *  @brief This class is used as the parameter of the method_call function.
 *         method_call function is contained in the AMAPIHelper class which
 *         is used to interact with oryx.
 */
class AMIApiPlaybackAudioFileList
{
  public:
    /*! Create function.
     */
    static AMIApiPlaybackAudioFileList* create();
    /*! Create function.
     */
    static AMIApiPlaybackAudioFileList* create(AMIApiPlaybackAudioFileList*
                                               audio_file);
    /*! Destructor function
     */
    virtual ~AMIApiPlaybackAudioFileList(){}
    /*! This function is used to add file to the class.
     *  @param The param is file name. The max length of it is 490 bytes
     *  @return true if success, otherwise return false.
     */
    virtual bool add_file(const std::string &file_name) = 0;
    /*! This function is used to get file which was added to this class before.
     *  @param The param is file number. For example, 1 means the first file.
     *  @return true if file number is valid, otherwise false.
     */
    virtual std::string get_file(uint32_t file_number) = 0;
    /*! This function is used to get how many files in the class.
     *  @return Return how many files contained in this class.
     */
    virtual uint32_t get_file_number() = 0;
    /*! This function is used to check the file list full or not.
     * @return if it is full, return true, otherwise return false;
     */
    virtual bool is_full() = 0;
    /*! This function is used to get the whole file list.
     * @return return the address of the file list.
     */
    virtual char* get_file_list() = 0;
    /*! This function is used to get the size of file list.
     * @return return the size of the file list.
     */
    virtual uint32_t get_file_list_size() = 0;
    /*! This function is used to clear all files in the class.
     */
    virtual void clear_file() = 0;
};

/*! @class AMIApiMediaEventStruct
 *  @brief This class is used as the parameter of the method_call function.
 *         method_call function is contained in the AMAPIHelper class which
 *         is used to interact with oryx.
 */
class AMIApiMediaEvent
{
  public :
    /*! Create function.
     */
    static AMIApiMediaEvent* create();
    /*! Destructor function
     */
    virtual ~AMIApiMediaEvent() {}
    /*! This function is used to set event attr as mjpeg.
     */
    virtual void set_attr_mjpeg() = 0;
    /*! This function is used to set event attr as h264 or h265.
     */
    virtual void set_attr_h26X() = 0;
    /*! This function is used to get attr.
     *  @return true if attr is h264 or h265, otherwise return false.
     */
    virtual bool is_attr_h26X() = 0;
    /*! This function is used to get attr.
     *  @return true if attr is mjpeg, otherwise return false.
     */
    virtual bool is_attr_mjpeg() = 0;
    /*! This function is used to set event id.
     *  @param The param is event id.
     *  @return true if success, otherwise return false.
     */
    virtual bool set_event_id(uint32_t event_id) = 0;
    /*! This function is used to get event id.
     *  @return event id.
     */
    virtual uint32_t get_event_id() = 0;
    /*! This function is used to set pre current pts number.
     *  @param The param is number.
     *  @return true if success, otherwise return false.
     */
    virtual bool set_pre_cur_pts_num(uint8_t num) = 0;
    /*! This function is used to get pre current pts number.
     *  @return pre_cur_pts_num.
     */
    virtual uint8_t get_pre_cur_pts_num() = 0;
    /*! This function is used to set after current pts number.
     *  @param The param is number.
     *  @return true if success, otherwise return false.
     */
    virtual bool set_after_cur_pts_num(uint8_t num) = 0;
    /*! This function is used to get after current pts number.
     *  @return after_cur_pts_num.
     */
    virtual uint8_t get_after_cur_pts_num() = 0;
    /*! This function is used to set closest current pts number.
     *  @param The param is number.
     *  @return true if success, otherwise return false.
     */
    virtual bool set_closest_cur_pts_num(uint8_t num) = 0;
    /*! This function is used to get closest current pts number.
     *  @return closest currrent pts number.
     */
    virtual uint8_t get_closest_cur_pts_num() = 0;
    /*! This function is used to get all params data.
     *  @return params data.
     */
    virtual char* get_data()    = 0;
    /*! This function is used to get all params data size.
     *  @return params data size.
     */
    virtual uint32_t get_data_size() = 0;
};

/*!
 * @}
 */
#endif /* AM_API_MEDIA_H_ */
