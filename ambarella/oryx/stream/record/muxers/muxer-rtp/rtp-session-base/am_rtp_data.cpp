/*******************************************************************************
 * am_rtp_data.cpp
 *
 * History:
 *   2015-1-6 - [ypchang] created file
 *
 * Copyright (c) 2016 Ambarella, Inc.
 *
 * This file and its contents ("Software") are protected by intellectual
 * property rights including, without limitation, U.S. and/or foreign
 * copyrights. This Software is also the confidential and proprietary
 * information of Ambarella, Inc. and its licensors. You may not use, reproduce,
 * disclose, distribute, modify, or otherwise prepare derivative works of this
 * Software or any portion thereof except pursuant to a signed license agreement
 * or nondisclosure agreement with Ambarella, Inc. or its authorized affiliates.
 * In the absence of such an agreement, you agree to promptly notify and return
 * this Software to Ambarella, Inc.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF NON-INFRINGEMENT,
 * MERCHANTABILITY, AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL AMBARELLA, INC. OR ITS AFFILIATES BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; COMPUTER FAILURE OR MALFUNCTION; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/

#include "am_base_include.h"
#include "am_define.h"
#include "am_log.h"

#include "am_rtp_data.h"
#include "am_rtp_session_if.h"

AMRtpPacket::AMRtpPacket() :
  tcp_data(nullptr),
  udp_data(nullptr),
  total_size(0)
{}

uint8_t* AMRtpPacket::tcp()
{
  return tcp_data;
}

uint32_t AMRtpPacket::tcp_data_size()
{
  return total_size;
}

uint8_t* AMRtpPacket::udp()
{
  return udp_data;
}

uint32_t AMRtpPacket::udp_data_size()
{
  return (total_size - sizeof(AMRtpTcpHeader));
}

AMRtpData::AMRtpData() :
  pts(0LL),
  buffer(nullptr),
  packet(nullptr),
  owner(nullptr),
  id(0),
  buffer_size(0),
  data_size(0),
  payload_size(0),
  pkt_size(0),
  pkt_num(0),
  ref_count(0)
{}

AMRtpData::~AMRtpData()
{
  clear();
}

void AMRtpData::clear()
{
  delete[] buffer;
  delete[] packet;
  buffer = nullptr;
  packet = nullptr;
  owner  = nullptr;
  pts = 0LL;
  pkt_size = 0;
  pkt_num = 0;
  buffer_size = 0;
  data_size = 0;
  ref_count = 0;
}

bool AMRtpData::create(AMIRtpSession *session,
                       uint32_t datasize,
                       uint32_t payloadsize,
                       uint32_t packet_num)
{
  owner = session;
  payload_size = payloadsize;
  if (AM_LIKELY(datasize > 0)) {
    if (AM_UNLIKELY(datasize > buffer_size)) {
      delete[] buffer;
      buffer = nullptr;
      if (AM_LIKELY(buffer_size)) {
        INFO("ID: %02u, Prev buffer size: %u, current buffer size: %u",
              id, buffer_size, datasize);
      }
      buffer_size = ROUND_UP((5 * datasize / 4), 4);
    }
    data_size = datasize;
    if (AM_LIKELY(!buffer)) {
      buffer = new uint8_t[buffer_size];
    }
  }
  if (AM_LIKELY(packet_num > 0)) {
    if (AM_UNLIKELY(packet_num > pkt_size)) {
      delete[] packet;
      packet = nullptr;
      if (AM_LIKELY(pkt_size)) {
        INFO("ID: %02u, Prev packet num: %u, current packet num: %u",
             id, pkt_size, packet_num);
      }
      pkt_size = 5 * packet_num / 4;
    }
    pkt_num = packet_num;
    if (AM_LIKELY(!packet)) {
      packet = new AMRtpPacket[pkt_size];
    }
  }
  return (buffer && packet);
}

void AMRtpData::add_ref()
{
  ++ ref_count;
}

void AMRtpData::release()
{
  if (--ref_count <= 0) {
    owner->put_back(this);
    ref_count = 0;
  }
}
